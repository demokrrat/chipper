/* eslint-disable import/no-useless-path-segments */
import svg4everybody from 'svg4everybody';
import UIkit from 'uikit';
import Icons from './../../node_modules/uikit/dist/js/uikit-icons';
import {DOM} from './_const';

import './modules/_notification'

// need to disabled on bild
// import './modules/_custom-select'

import './modules/_input-mask'
import './modules/_range-slider'
import './modules/_accordion'
import './modules/_tabs'
import './modules/_card-slider'
import './modules/_drop-filter'
import './modules/_slider-product'
import './modules/_cabinet'
import './modules/_nice-scroll'
import './modules/_tags'
import './modules/_create-product'
import './modules/_card-type-define'
import './modules/_product-page'
import './modules/_aside-filter'
import './modules/_mobile-menu'
import './modules/_message'

import './modules/_form-drop-2'

UIkit.use(Icons);





svg4everybody();

const onContentLoaded = () => {
  let lazyImages = [].slice.call(document.querySelectorAll('img.lazy'));

  const lazyLoad = () => {
    lazyImages.forEach((lazyImage) => {
      const topBreakpoint = lazyImage.classList.contains('lazy_last') ?
                            window.innerHeight + 500 : window.innerHeight + 200;

      if ((lazyImage.getBoundingClientRect().top <= topBreakpoint
          && lazyImage.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazyImage).display !== 'none') {
        // eslint-disable-next-line
        lazyImage.src = lazyImage.dataset.src;
        // eslint-disable-next-line
        lazyImage.srcset = lazyImage.dataset.srcset;
        lazyImage.classList.remove('lazy');

        lazyImages = lazyImages.filter((image) => {
        return image !== lazyImage;
        });

        if (lazyImages.length === 0) {
          DOM.document.removeEventListener('scroll', lazyLoad);
          DOM.window.removeEventListener('resize', lazyLoad);
          DOM.window.removeEventListener('orientationchange', lazyLoad);
        }
      }
    });
  };

  DOM.doc.addEventListener('scroll', lazyLoad);
  DOM.win.addEventListener('resize', lazyLoad);
  DOM.win.addEventListener('orientationchange', lazyLoad);
}



const onLoad = () => {

  const $darkE = document.createElement("div"); 
  const $out = document.getElementsByClassName('out');

  $($darkE).addClass('darknes')
  $out[0].appendChild($darkE);  
  

}

$(DOM.doc).ready(onLoad);
DOM.doc.addEventListener('DOMContentLoaded', onContentLoaded);

