(function($){
  var methods = {
      init: function (options){

          // add our input tag
          $(this).html(`<span class="tags-wrapper"></span><input text="" class="add-tags" name="add-tags" id="add-tags" placeholder="${options.tagInputPlaceholder}" value="" autocomplete="off"/>`)
          
          // initialize tags
          $.each(options.initialTags, function(_, value){
              addTag(value, options.tagBackgroundColor, options.tagColor, options.tagBorderColor, options.tagHiddenInput, options.maxTagsLength, options.tagsNotificationHide)
              console.log(11111)
          });



          const $notif = $(options.tagHiddenInput).siblings('.js-tags__notification');

          if($(`[tag-title]`).length >= options.tagsNotificationHide ){
            $notif.hide()
            // console.log(1)
        } else {
            $notif.show()
            // console.log(0)
        }


          // focus on our input on the container clicked
          $(this).parent().click(function(){
              $(".add-tags").focus();
          })

           // add tag on key down
          $(".add-tags").keydown(function (evt) {
              if ((evt.keyCode == 32) | (evt.keyCode == 9)) {
                //   console.log(options.maxTagsLength+' - ' + $(`[tag-title]`).length)
                if($(`[tag-title]`).length <= options.maxTagsLength-1 ){
                  var tag = $.trim($(this).val());
                  if (tag.length < 1) {
                      return false;
                  }
                  addTag(tag, options.tagBackgroundColor, options.tagColor, options.tagBorderColor, options.tagHiddenInput, options.maxTagsLength, options.tagsNotificationHide)
                  $(this).val("");
                  $(this).focus();

                } 
                if($(`[tag-title]`).length >= options.tagsNotificationHide ){
                    $notif.hide()
                    // console.log(1)
                } else {
                    $notif.show()
                    // console.log(0)
                }

                
            }
          });

           // remove tag on close icon click
          $(document).on("click", ".tag-remove", function () {
              var tag = $(this).attr("tag");
              $(`[tag-title='${tag}']`).remove();
              copyTags(options.tagHiddenInput);
              $(".tags-wrapper").focus();
              if($(`[tag-title]`).length >= options.tagsNotificationHide ){
                $notif.hide()
                // console.log(1)
                } else {
                    $notif.show()
                    // console.log(0)
                }
    
    
          });

          return $(this).css({
              "border-color": options.tagContainerBorderColor,
              "border-width": ".1em",
              "border-style": "solid",
          });
      }
  };

  // add tag
  function addTag(tagName, tagBackgroundColor, tagColor, tagBorderColor, tagHiddenInput, maxTagsLength){
      if(!$(`[tag-title='${tagName}']`).length){
        var tagHTML = `<span style="background-color: ${tagBackgroundColor}; color: ${tagColor}; border-color: ${tagBorderColor}" class="tags" tag-title="${tagName}">
        #${tagName}
        <a title="Remove tag" class="tag-remove" tag="${tagName}">
            <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M3.81508 3.10532L6.72021 0.200195L7.55008 1.03006L4.64495 3.93519L7.55008 6.84032L6.72021 7.67019L3.81508 4.76506L0.909947 7.67019L0.0800781 6.84032L2.98521 3.93519L0.0800781 1.03006L0.909947 0.200195L3.81508 3.10532Z" fill="#1C1C1C"/>
            </svg>
            
        </a>
        </span>`;
        $(".tags-wrapper").append(tagHTML);
        copyTags(tagHiddenInput)
      }
  }

  // add tag to the hidden input
  function copyTags(tagHiddenInput){
      var listOfTags = [];
      $(".tags").each(function () {
        listOfTags.push($(this).text().trim());
      });
      tagHiddenInput.val(listOfTags.join(","));
      console.log($('.form__input-tags').val())
  }

  // declare our tag input plugin
  $.fn.TagsInput = function (methodOrOptions){
      if( typeof methodOrOptions === 'object' || ! methodOrOptions ){
          return methods.init.apply(this, arguments)
      }
      else if ( methods[methodOrOptions] ) {
          return methods[ methodOrOptions ].apply(this, Array.prototype.slice.call(arguments, 1 ));
      }
      else {
          $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.TagsInput' );
      }  
  }
})( jQuery );