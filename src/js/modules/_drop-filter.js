/* eslint-disable prefer-const */
/* eslint-disable no-unused-vars */
/* eslint-disable no-template-curly-in-string */
/* eslint-disable func-names */
import UIkit from 'uikit';


const filterModal = UIkit.modal('#search-main', {
  stack: true
});


UIkit.util.on('#search-main', 'shown', function (e) {
  setTimeout(() => {
    window.dispatchEvent(new Event('resize'));
  }, 20); 

});
const dropFilter = UIkit.dropdown($('.js-dropdown-filter-main'), {
  pos: 'bottom-left',
  offset: '56',
  boundary: '.search-full__bar > .uk-container .search-full__bar',
  mode: 'click'
});

const $dropSearchBody = $('.search-full__drop-body');

const checkFilterSelect = function() {
  const $item = $('[data-select-filters]');

  let data = $item.attr('data-select-filters');
  let result = (data === 'true');
  // console.log(data)
  return result;
}

// let cond = null;
// const checkClickInner = ($item) => {
  
//   if ( $item.closest('.js-dropdown-filter-main-body') ) {
//     cond = true
//   } else {
//     cond = false
//   }
//   console.log(cond)

//   return cond
// }

// $( ".js-dropdown-filter-main-body" ).on( "click",  function(e) {
//   checkClickInner($(e.target))
// });
let dropSost = false
UIkit.util.on('.js-dropdown-filter-main', 'beforeshow', function (e) {
  if ( checkFilterSelect() ) {
    dropSost = true
  } else {
    $('.js-dropdown-filter-main-body').show();
    $('.js-dropdown-filter-main-body').removeClass('is-hide');
  }

});

UIkit.util.on('.js-dropdown-filter-main', 'beforehide', function (e) {
  if ( checkFilterSelect() ) {
    // e.preventDefault();
    
    // dropSost = false;

    // setTimeout(() => {
        
    //   if ( !$('.js-dropdown-filter-main-body').is(':visible') ) {
    //     dropSost = true;
    //   }
    //   console.log(`set tim ${dropSost}`)
    // }, 20);

    // setTimeout(() => {
    //   if ( !dropSost ) {
    //     $dropSearchBody.hide();
    //     $('.js-show-main-filter').removeClass('uk-open')
    //   }
    //   dropSost = false;
    //   console.log(`set tim2 ${dropSost}`)
    // }, 30);
        
    //   console.log(`not hide ${dropSost}`)
  }
});


$(document).on("click",".uk-open.js-show-main-filter",function() {
  if ( checkFilterSelect() ) {
    
    // console.log(`3 ${dropSost}`)
    // if ( $('.js-dropdown-filter-main-body').is(':visible') ) {
      
    //   dropSost = false;
    //   $('.js-dropdown-filter-main-body').hide();
    //   $('.js-dropdown-filter-main-body').addClass('is-hide');
    //   console.log('click hide')
    //   } else if ( !$('.js-dropdown-filter-main-body').is(':visible') && !dropSost  ) {
    //     setTimeout(() => {
    //       $('.js-dropdown-filter-main-body').show();
    //       $('.js-dropdown-filter-main-body').removeClass('is-hide');
    //       dropSost = true;
    //       console.log('1')
    //     }, 50);
     
        
    //   } else {
    //     dropSost = true;
    //   }
  }
});


$(document).on("click",".js-clear-search",function() {
  $('.js-main-search').val('')
});


$(document).on("click",".js-clear-search",function() {
  $('.js-main-search').val('')
});
// js-show-search
let $hasActive = ''
UIkit.util.on('#search-main', 'show', function () {
  // do something

  $hasActive = $('.panel-footer__icon.is-active').index();
  $('.panel-footer__icon').removeClass('is-active');
  $('.js-show-search').addClass('is-active');


});

UIkit.util.on('#search-main', 'hide', function () {
  // do something

  $('.js-show-search').removeClass('is-active');
  $('.panel-footer__icon').eq($hasActive).addClass('is-active')


});