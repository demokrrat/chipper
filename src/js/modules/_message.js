/* eslint-disable func-names */
$(document).ready(function () {

  const $searchMessMob = $('.js-search-message');
  const $searchMessMobShow = '.js-show-search-message-search'
  const $searchMessMobHide = '.js-hide-search-message-search'




  $(document).on("click",$searchMessMobShow,function() {
    if ( !$searchMessMob.hasClass('is-active') ) {
      $searchMessMob.addClass('is-active')
    }
  });
  $(document).on("click",$searchMessMobHide,function() {
    if ( $searchMessMob.hasClass('is-active') ) {
      $searchMessMob.removeClass('is-active')
    }
  });

  $(document).on("click", '.js-to-show-on-mobile-message',function() {
    if ( !$('.js-show-on-mobile-message').hasClass('is-active') ) {
      $('.js-show-on-mobile-message').addClass('is-active')
    }
  });
  $(document).on("click", '.js-hide-on-mobile-message',function() {
    if ( $('.js-show-on-mobile-message').hasClass('is-active') ) {
      $('.js-show-on-mobile-message').removeClass('is-active')
    }
  });

});