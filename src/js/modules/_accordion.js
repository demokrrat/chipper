/* eslint-disable no-else-return */
/* eslint-disable no-unused-vars */
/* eslint-disable prefer-const */
/* eslint-disable func-names */
import UIkit from 'uikit';
import accordion from 'jq-accordion';

$(document).ready(function () {

  UIkit.accordion('.js-filter-accordion', {
    toggle: '> .aside-filter__head',
    content: '> .aside-filter__body'
  });

  UIkit.accordion('.js-accordion-filter-cat', {
    toggle: '> .aside-filter__body-head',
    content: '> .aside-filter__body-body'
  });


  
  $('.accordion').accordion({
  });

  
});