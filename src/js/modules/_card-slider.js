/* eslint-disable object-shorthand */
/* eslint-disable prefer-const */
/* eslint-disable func-names */
/* eslint-disable no-unused-vars */
import {Swiper, Pagination} from 'swiper';
import customPagination from './_custom-pagina';

Swiper.use([Pagination]);

  // eslint-disable-next-line func-names
$(document).ready(function () {


  if (  $('.card-item__img').length) {

  
  
  // eslint-disable-next-line no-unused-vars
  $.each( $('.card-item__img'), function (indexInArray, valueOfElement) { 
    const $slider = $(this).find('.swiper-container');
    const $pagina = $(this).find('.card-item__img-pagination')[0];

    const $key = $(this).find('[data-key-pagina]')
    const keyText = $key.data('key-pagina')
  
    const slider = new Swiper($slider[0], {
      loop: true,
      slidesPerView: 1,
      pagination: {
        el: $pagina,
        type: 'custom',
        renderCustom: customPagination,
        clickable: true
      },
      on: {
        init: function () {
          
         
          setTimeout(() => {
            const $paginaText = $(valueOfElement).find('.pagination__text')
            // console.log($paginaText )
            $paginaText.text(keyText)
          }, 300);
         
        },
      },
      
    });

    slider.on('slideChange', function () {
      const $paginaText = $(valueOfElement).find('.pagination__text')
            // console.log($paginaText )
            $paginaText.text(keyText)
    });


   

  });

  $(document).on("click", $('.card-item__img-pagination .pagina-bulets__item'),function() {
    const index = $(this).index();
    // console.log(index)
});

}
  
});

