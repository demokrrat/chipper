/* eslint-disable func-names */
import {Thumbs, Navigation, Swiper} from 'swiper';

Swiper.use([Thumbs, Navigation]);

$(document).ready(function () {
  

  const galleryThumbs = new Swiper('.js-small-product-slider', {
    spaceBetween: 0,
    slidesPerView: 'auto',
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    navigation: {
      nextEl: '.detailed-product__slider-next',
      prevEl: '.detailed-product__slider-prev',
    },
  });
  // eslint-disable-next-line no-unused-vars
  const galleryTop = new Swiper('.js-big-product-slider', {
    slidesPerView: 1,
    navigation: {
      nextEl: '.detailed-product__slider-next',
      prevEl: '.detailed-product__slider-prev',
    },
    thumbs: {
      swiper: galleryThumbs
    }
  });


  

  const gallerySeeMore = new Swiper('.js-see-more-slider', {
    spaceBetween: 18,
    breakpoints: {
      // when window width is >= 320px
      320: {
        
        slidesPerView: 'auto',
        freeMode: true
      },
      // when window width is >= 480px
      767: {
       
        slidesPerView: 2,
        freeMode: false
      },
    }
  });



});

  
