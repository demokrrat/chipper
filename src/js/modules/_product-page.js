/* eslint-disable no-restricted-syntax */
/* eslint-disable no-unused-vars */
/* eslint-disable func-names */
import UIkit from 'uikit';

$(document).ready(function () {

  $.each($('.js-product-select-property'), function (indexInArray, valueOfElement) { 

    const $current = $(this).find('.js-product-select-property-text');
    const $target = $(this).find('[data-text-name]');
    $target.click(function (e) { 
      e.preventDefault();

      

      const text = $(e.target).data('text-name');
      $current.text(text)
      
      $target.removeClass('is-active')
      $(e.target).addClass('is-active')
    });

     
  });


  const setTabsToAccordion = (elms, elmsto) => {

    const $links = elms.find('.detailed-product__tabs-link');
    const $text = elms.find('.js-tabs-body .detailed-product__tabs-content');

    

    const $accItem = elmsto.find('li');
    let $accText = []
  

    // const links = []
    let $linksTo = [];

    const $cloneAccTitle = $accItem.clone()
    $cloneAccTitle.removeClass('uk-open')
    const clone = () => {

      for (const [i, v] of [...$links].entries()) {
        $cloneAccTitle.appendTo(elmsto);

        $accText = elmsto.find('.js-from-tabs-content');
        $linksTo = elmsto.find('.js-from-tabs-title');
        
        $linksTo.eq(i).html(v.textContent)
        $accText.eq(i).html([...$text][i].innerHTML)
      }

     
    }

    clone();


  } 

  setTabsToAccordion($('.detailed-product__tabs'), $('.js-from-tabs-list'));


  UIkit.util.on('.sharing-drop__wrapper', 'show', function () {
    // do something
    $('.js-share-button').addClass('is-active');

  });
  UIkit.util.on('.sharing-drop__wrapper', 'hide', function () {
    // do something
    $('.js-share-button').removeClass('is-active');

  });

  // copy link

  function copyToClipboard(text) {
      const dummy = document.createElement("textarea");
      // to avoid breaking orgain page when copying more words
      // cant copy when adding below this code
      // dummy.style.display = 'none'
      document.body.appendChild(dummy);
      // Be careful if you use texarea. setAttribute('value', value), which works with "input" does not work with "textarea". – Eduard
      dummy.value = text;
      dummy.select();
      document.execCommand("copy");
      document.body.removeChild(dummy);
  }

  $(document).on("click",".js-copy-link",function(e) {
    e.preventDefault()
    const link = $(this).data('link-product')
    copyToClipboard(link)
  });


  
});