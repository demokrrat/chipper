/* eslint-disable no-void */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-cond-assign */
/* eslint-disable vars-on-top */
/* eslint-disable no-return-assign */
/* eslint-disable prefer-template */
/* eslint-disable prefer-destructuring */
/* eslint-disable func-names */
/* eslint-disable object-shorthand */
/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
/* eslint-disable no-var */

import Dropzone from 'dropzone'


$(document).ready(function () {
  Dropzone.autoDiscover = false;
 
  if ( $('.js-form-drop').length ) {

    const uploadUrlFirst = $('.js-form-drop').data('url-to-upload')

    const myDropzone = new Dropzone(".js-form-drop", { 
      addRemoveLinks: true,
      autoProcessQueue:false,
      maxFilesize: 10,
      acceptedFiles: ".jpeg,.jpg,.png,.gif",
      url: uploadUrlFirst,
      removedfile: function(file) {
        var name = file.name; 
        
        $.ajax({
          type: 'POST',
          url: uploadUrlFirst,
          data: {name: name,request: 2},
          sucess: function(data){
            console.log('success: ' + data);
          }
        });
        var _ref;
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
      }
    });

    const closestForm = $('.js-form-drop').closest('form')
    $(document).on('submit',closestForm,function(e){
      // code
      // e.preventDefault()
      myDropzone.processQueue();  
   });

   
 

  }

  if ( $('.js-img-upload-second').length ) {

    const uploadUrlsecond = $('.js-img-upload-second').data('url-to-upload')

    const myDropzoneSecond = new Dropzone(".js-img-upload-second", { 
      autoQueue: false,
  
      addRemoveLinks: true,
      maxFilesize: 10,
      clickable: ".js-img-upload-second-button",
      acceptedFiles: ".jpeg,.jpg,.png,.gif",
      url: uploadUrlsecond,
      removedfile: function(file) {
        var name = file.name; 
        
        $.ajax({
          type: 'POST',
          url: uploadUrlsecond,
          data: {name: name,request: 2},
          sucess: function(data){
             console.log('success: ' + data);
          }
        });
        var _ref;
         return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
      },
      init: function() {
        this.on("sendingmultiple", function(data, xhr, formData) {
          console.log(formData)
      });
      }
    }); 
    
  }
 
  
});


console.log(1112213213211)