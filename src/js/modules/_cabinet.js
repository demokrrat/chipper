/* eslint-disable vars-on-top */
/* eslint-disable no-var */
/* eslint-disable eqeqeq */
/* eslint-disable yoda */
/* eslint-disable no-undef */
/* eslint-disable func-names */
import UIkit from 'uikit';

$(document).ready(function () {
    $( ".modal-small .add-comment-modal" ).click(function() {
        $(this).siblings('.comment-block').addClass('open');
        $(this).addClass('hide');
        $(this).parents('.comment-modal').addClass('open');
        $('.modal-photo').addClass('hide');
    });
    $( ".modal-small .comment-block .close" ).click(function() {
        $(this).parents('.comment-block').removeClass('open');
        $('.modal-photo').removeClass('hide');
        $('.modal-small .add-comment-modal').removeClass('hide');
        $(this).parents('.comment-modal').removeClass('open');
    });

    UIkit.util.on('#modal-following', 'show', function () {
        window.dispatchEvent(new Event('resize'));        
    });
    UIkit.util.on('#modal-followers', 'show', function () {
        window.dispatchEvent(new Event('resize'));        
    });

    $('.profile-table .title-settings .edit-btn').click(function () {
        $(this).parents('.profile-group').addClass('edit');
    })
    $('.profile-table .apply-group button').click(function () {
        $(this).parents('.profile-group').removeClass('edit');
    });

    $('.toggle-password').click(function(e){
        e.preventDefault();

        var passInput = $(this).parents('.input-group').find('.password-input');

        if (passInput.attr('type') == 'password'){
            $(this).addClass('active');
            passInput.attr('type', 'text');
        } else {
            $(this).removeClass('active');
            passInput.attr('type', 'password');
        }

    });
});