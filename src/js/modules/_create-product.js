/* eslint-disable func-names */
$(document).ready(function () {

  const createCopyNode = function(elm) {
    const $elm = elm.clone();
    return $elm;
  }

  const $elemDefault = createCopyNode($('.js-product-detail-copy'));

  $('.js-add-product-copy').click(function (e) { 
    e.preventDefault();

    // $elemDefault.prependTo($(this))
    // console.log($elemDefault.clone())
    $('.js-product-detail-copy-to').prepend($elemDefault.clone())
    
  });

  
});