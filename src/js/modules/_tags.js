import '../lib/taginput'

const tagsList = $('.form__tags').data('tags')
console.log(tagsList)
$('.form__tags').TagsInput({

  // placeholder text
  tagInputPlaceholder:'Введите тег',

  // hidden input
  tagHiddenInput: $('.form__input-tags'),

  // border color
  tagContainerBorderColor: '',
  
  // background color
  tagBackgroundColor: '',

  // tag color
  tagColor: '',

  // border color of tags
  tagBorderColor: '',
  
  maxTagsLength: 9,

  tagsNotificationHide: 2,

  initialTags: tagsList

});

