/* eslint-disable func-names */
/* eslint-disable no-console */

$(document).on("click",".js-notification",function(e) {

  console.log(e.target);

  if ( $(e.target).hasClass('js-close-notification') || $(e.target).is('use') ) {
    $(this).toggleClass('is-active');
    $('.header, .header__inner').removeClass('notification-active');
  }
  
});

$(document).ready(function () {
  if ( $('.notification').hasClass('is-active') ) {
    $('.header, .header__inner').addClass('notification-active');
  }
});