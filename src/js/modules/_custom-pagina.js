/* eslint-disable no-console */
const customPagination = (swiper, current, total) => {
  const checkCurrent = current;
  const checkTotal = total;

 
  let text = ''
  
  for (let i = 1; i <= total; i++) {
    
    if (current === i) {
      text += `<span class="pagina-bulets__item is-active"></span>`
    } else {
      text += `<span class="pagina-bulets__item"></span>`
    }

  }
  
  return `<div class="pagina-bulets"> ${text} </div>
          <span class="pagination__current">${checkCurrent}</span> /
          <span class="pagination__total">${checkTotal}</span>
          <span class="pagination__text"></span>`;
};


export default customPagination;