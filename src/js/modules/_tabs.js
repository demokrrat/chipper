/* eslint-disable func-names */
import UIkit from 'uikit';


$('.js-tabs').each(function (index, element) {
  // element == this
  
  const $body = $(element).find('.js-tabs-body');
  const $navItem = $(element).find('.js-tabs-header .detailed-product__tabs-link')
  const $nav = $(element).find('.js-tabs-header')
  $body.addClass(`js-elem-body-${index}`);
  $navItem.addClass(`js-elem-item-${index}`);
  $nav.addClass(`js-elem-nav-${index}`);

  UIkit.switcher(`.js-elem-nav-${index}`, {
    connect: `.js-elem-body-${index}`,
    toggle: `.js-elem-item-${index}`,
    // animation: 'uk-animation-fade'
  });


  
});